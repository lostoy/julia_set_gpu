//
// Created by yingwei on 4/27/18.
//
#define WIDTH 32//1024
#define HEIGHT 32//768
#define MAX_ITER 200
#define C_RE (-0.8)
#define C_IM (0.156)
#define THRESH 1000
#include <cuda.h>
//#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <assert.h>
#include <sys/time.h>
#include <cuda.h>


// External function definitions

void checkCUDAError(const char *msg)
{
    cudaError_t err = cudaGetLastError();
    if( cudaSuccess != err)
    {
        fprintf(stderr, "Cuda error: %s: %s.\n", msg,
                cudaGetErrorString( err) );
        exit(EXIT_FAILURE);
    }
}

struct MyComplexf
{
    __device__ MyComplexf(float x, float y):re(x), im(y){};
    __device__ struct MyComplexf conj()
    {
        return MyComplexf(re, -im);
    }
    __device__ struct MyComplexf operator * (const struct MyComplexf& o)
    {
        return MyComplexf(re*o.re-im*o.im, re*o.im+im*o.re);
    }
    __device__ struct MyComplexf operator + (const struct MyComplexf& o)
    {
        return MyComplexf(re+o.re, im+o.im);
    }
    float re, im;

};
//using namespace cv;
__device__ int julia(int i, int j)
{
    float y = ((float)(i - HEIGHT / 2)) / HEIGHT;
    float x = ((float)(j - WIDTH / 2)) / WIDTH;
    MyComplexf z(x, y);

    for (int r = 0; r < MAX_ITER; ++r) {
        z = z * z + MyComplexf(C_RE, C_IM);
        if ((z * z.conj()).re >= THRESH)
            return false;
    }
    return true;
}
__global__ void julia_kernel(unsigned char* img)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    img[y*WIDTH + x] = julia(y, x);

}

int main()
{
    unsigned char *img_d;
    cudaMalloc((void**) &img_d, WIDTH*HEIGHT);
    checkCUDAError("Error allocating device memory for matrix img_d");
    dim3 grid(WIDTH, HEIGHT);
    dim3 threads(1, 1);
    julia_kernel<<<grid, threads>>>(img_d);

    unsigned char* img_h = (unsigned char *) malloc(WIDTH*HEIGHT);
    cudaMemcpy(img_h, img_d, WIDTH*HEIGHT, cudaMemcpyHostToDevice);
    checkCUDAError("Error copying device memory for matrix img_d");
    return 0;
}