#include <iostream>
#include <opencv2/opencv.hpp>
#define WIDTH 1024
#define HEIGHT 768
#define MAX_ITER 200
#define C_RE (-0.8)
#define C_IM (0.156)
#define THRESH 1000

using namespace cv;

bool julia(int i, int j)
{
    float y = ((float)(i - HEIGHT / 2)) / HEIGHT;
    float x = ((float)(j - WIDTH / 2)) / WIDTH;
    Complexf z(x, y);

    for (int r = 0; r < MAX_ITER; ++r) {
        z = z * z + Complexf(C_RE, C_IM);
        if ((z * z.conj()).re >= THRESH)
            return false;
    }
    return true;
}
int main() {
    Mat img(HEIGHT, WIDTH, CV_8UC1);
    for (int i = 0; i < HEIGHT; ++i)
        for (int j = 0; j < WIDTH; ++j)
            img.at<unsigned char>(i, j) = julia(i, j)*255;
    imshow("img", img);
    waitKey(0);
    return 0;
}